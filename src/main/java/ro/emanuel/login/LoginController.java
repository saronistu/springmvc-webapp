package ro.emanuel.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import ro.emanuel.login.LoginService;

@Controller
@SessionAttributes("name")
public class LoginController {
	
	@Autowired
	LoginService service;

	@RequestMapping (value = "/", method = RequestMethod.GET)
	public String loginViewGet() {
		return "login";
	}
	
	@RequestMapping (value = "/", method = RequestMethod.POST)
	public String loginViewPost(@RequestParam String name, @RequestParam String password, ModelMap model) {
		if (!service.validateUser(name, password)) {
			model.put("error", "Invalid credentials");
			return "login";
		}
			model.put("name", name);
			model.put("password", password);
			return "welcome";
	}
}
